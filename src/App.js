import React from 'react';
import './App.css';
import 'tachyons';
// import witch from './card-art/witch.jpg';
import { cards } from './card_details';
import { games } from './set_games';
import CardList from './CardList';
// import InputRange from 'react-input-range';
import './Slider.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    // Select 10 random cards and generate a list
    // Use just the base set
    // let set = cards.base.core;
    let generation = this.generate();
    /*
      APP STATE
    */
    this.state = {
      draw: generation.draw,
      buy: generation.buy,
      tempo: generation.tempo,
      offence: generation.offence,
      trash: generation.trash,
      money: generation.money,
      victory: generation.money,
      cardSelection: generation.card_list,
      name: 'Random Generation'
    };

  }

  /*
    Takes an array of cards and returns an object containing the 
    sum of all the statistics and a sorted version of the card list 
    itself.
    @param: card_list Array
    @returns: {draw, buy, tempo, offence, trash, money, victory, card_list }
  */
  generateStats(card_list){
    card_list.sort((a, b) => {return a.cost - b.cost});
    let draw = 0
    let buy = 0;
    let tempo = 0;
    let offence = 0;
    let trash = 0; 
    let money = 0;
    let victory = 0;
    for (let i = 0; i < card_list.length; i++) {
      const card = card_list[i];
      draw += card.draw;
      buy += card.buy;
      tempo += card.tempo;
      offence += card.offence;
      trash += card.trash;
      money += card.money;
      victory += card.victory;
    }

    let generation_set = {draw, buy, tempo, offence, trash, money, victory, card_list };

    return generation_set;
  }

  generate(){
    let pickedNumbers = [];
    let randomSelection = [];
    let set = cards.base.core;

    while(randomSelection.length < 10){
      let rand = set[Math.floor(Math.random() * set.length)];
      if(!pickedNumbers.includes(rand)){
        randomSelection.push(rand);
        pickedNumbers.push(rand);
      } 
    }

    let card_set = this.generateStats(randomSelection);
    card_set.name = 'Random Generation';

    return card_set;
  }

  generateSetGame(){
    const base_games = games.base;
    const base_set = cards.base.core;
    let selection = []

    // Pick a random game
    const chosen_game = base_games[Math.floor(Math.random() * base_games.length)];
    
    // Create the selection from the card details list
    chosen_game.cards.forEach(card_name => {
      let card = base_set.find((element) => {
        return element.name === card_name;
      });
      if(!card){
        console.log('No card found with name ' + card_name);
      }
      selection.push(card);
    });


    let card_set = this.generateStats(selection);
    card_set.name = chosen_game.name;
    console.log(card_set);
    return card_set;
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h2>Dominion Generator</h2>
        </header>
        <section className='App-form pb3'>
          <div className='customForm'>
            <article class="cf w-70 center">
              <div class="fl w-50 tc">
              <div href="#" className="f2 grow no-underline br-pill ph3 pv2 mb2 dib white bg-hot-pink" 
                    onClick={()=> {
                        let {draw, buy, tempo, offence, trash, money, victory, card_list, name } = this.generate();
                        this.setState({
                          cardSelection : card_list,
                          draw: draw,
                          buy: buy,
                          tempo: tempo,
                          offence: offence,
                          trash: trash,
                          money: money,
                          victory: victory,
                          name: name
                        })
                      }}>
              Ramdomise
            </div>
              </div>
              <div class="fl w-50 tc">
              <div className="f2 grow no-underline br-pill ph3 pv2 mb2 dib white bg-hot-pink" 
                    onClick={()=> {
                        let {draw, buy, tempo, offence, trash, money, victory, card_list, name } = this.generateSetGame();
                        this.setState({
                          cardSelection : card_list,
                          draw: draw,
                          buy: buy,
                          tempo: tempo,
                          offence: offence,
                          trash: trash,
                          money: money,
                          victory: victory,
                          name: name
                        })
                      }}>
              Known Games
            </div>
              </div>
            </article>
            <div className="tc">
              <table className="f3 w-70 mw8 center pa3 ma3 br3 bg-near-white shadow-3">
                <thead>
                  <th>Draw</th>
                  <th>Buy</th>
                  <th>Tempo</th>
                  <th>Offence</th>
                  <th>Trash</th>
                  <th>Money</th>
                  <th>Victory</th>
                </thead>
                <tbody>
                  <td>{this.state.draw}</td>
                  <td>{this.state.buy}</td>
                  <td>{this.state.tempo}</td>
                  <td>{this.state.offence}</td>
                  <td>{this.state.trash}</td>
                  <td>{this.state.money}</td>
                  <td>{this.state.victory}</td>
                </tbody>
              </table>
            </div>
          </div>
        </section>
        <section className='App-body'>
          <h1>{this.state.name}</h1>
          <CardList cards={this.state.cardSelection} />
        </section>
      </div>
    );
  }

}

export default App;