export const cards = [
        {
          "name": "abandonedmine"
        },
        {
          "name": "advance"
        },
        {
          "name": "adventurer"
        },
        {
          "name": "advisor"
        },
        {
          "name": "alchemist"
        },
        {
          "name": "alms"
        },
        {
          "name": "altar"
        },
        {
          "name": "ambassador"
        },
        {
          "name": "amulet"
        },
        {
          "name": "annex"
        },
        {
          "name": "apothecary"
        },
        {
          "name": "apprentice"
        },
        {
          "name": "aqueduct"
        },
        {
          "name": "archive"
        },
        {
          "name": "arena"
        },
        {
          "name": "armory"
        },
        {
          "name": "artificer"
        },
        {
          "name": "artisan"
        },
        {
          "name": "bagofgold"
        },
        {
          "name": "baker"
        },
        {
          "name": "ball"
        },
        {
          "name": "banditcamp"
        },
        {
          "name": "banditfort"
        },
        {
          "name": "bandit"
        },
        {
          "name": "bandofmisfits"
        },
        {
          "name": "bank"
        },
        {
          "name": "banquet"
        },
        {
          "name": "baron"
        },
        {
          "name": "basilica"
        },
        {
          "name": "baths"
        },
        {
          "name": "battlefield"
        },
        {
          "name": "bazaar"
        },
        {
          "name": "beggar"
        },
        {
          "name": "bishop"
        },
        {
          "name": "blackmarket"
        },
        {
          "name": "bonfire"
        },
        {
          "name": "bordervillage"
        },
        {
          "name": "borrow"
        },
        {
          "name": "bridge"
        },
        {
          "name": "bridgetroll"
        },
        {
          "name": "bureaucrat"
        },
        {
          "name": "butcher"
        },
        {
          "name": "cache"
        },
        {
          "name": "candlestickmaker"
        },
        {
          "name": "capital"
        },
        {
          "name": "caravanguard"
        },
        {
          "name": "caravan"
        },
        {
          "name": "cartographer"
        },
        {
          "name": "castles"
        },
        {
          "name": "catacombs"
        },
        {
          "name": "catapultrocks"
        },
        {
          "name": "cellar"
        },
        {
          "name": "champion"
        },
        {
          "name": "chancellor"
        },
        {
          "name": "chapel"
        },
        {
          "name": "chariotrace"
        },
        {
          "name": "charm"
        },
        {
          "name": "city"
        },
        {
          "name": "cityquarter"
        },
        {
          "name": "coinoftherealm"
        },
        {
          "name": "colonnade"
        },
        {
          "name": "colony"
        },
        {
          "name": "conquest"
        },
        {
          "name": "conspirator"
        },
        {
          "name": "contraband"
        },
        {
          "name": "copper"
        },
        {
          "name": "coppersmith"
        },
        {
          "name": "councilroom"
        },
        {
          "name": "counterfeit"
        },
        {
          "name": "countinghouse"
        },
        {
          "name": "count"
        },
        {
          "name": "courtyard"
        },
        {
          "name": "crossroads"
        },
        {
          "name": "crown"
        },
        {
          "name": "cultist"
        },
        {
          "name": "curse"
        },
        {
          "name": "cutpurse"
        },
        {
          "name": "dameanna"
        },
        {
          "name": "damejosephine"
        },
        {
          "name": "damemolly"
        },
        {
          "name": "damenatalie"
        },
        {
          "name": "damesylvia"
        },
        {
          "name": "deathcart"
        },
        {
          "name": "defiledshrine"
        },
        {
          "name": "delve"
        },
        {
          "name": "develop"
        },
        {
          "name": "diadem"
        },
        {
          "name": "disciple"
        },
        {
          "name": "distantlands"
        },
        {
          "name": "doctor"
        },
        {
          "name": "dominate"
        },
        {
          "name": "donate"
        },
        {
          "name": "duchess"
        },
        {
          "name": "duchy"
        },
        {
          "name": "duke"
        },
        {
          "name": "dungeon"
        },
        {
          "name": "duplicate"
        },
        {
          "name": "embargo"
        },
        {
          "name": "embassy"
        },
        {
          "name": "encampmentplunder"
        },
        {
          "name": "enchantress"
        },
        {
          "name": "engineer"
        },
        {
          "name": "envoy"
        },
        {
          "name": "estate"
        },
        {
          "name": "expand"
        },
        {
          "name": "expedition"
        },
        {
          "name": "explorer"
        },
        {
          "name": "fairgrounds"
        },
        {
          "name": "familiar"
        },
        {
          "name": "farmersmarket"
        },
        {
          "name": "farmingvillage"
        },
        {
          "name": "farmland"
        },
        {
          "name": "feast"
        },
        {
          "name": "feodum"
        },
        {
          "name": "ferry"
        },
        {
          "name": "festival"
        },
        {
          "name": "fishingvillage"
        },
        {
          "name": "followers"
        },
        {
          "name": "foolsgold"
        },
        {
          "name": "forager"
        },
        {
          "name": "forge"
        },
        {
          "name": "fortress"
        },
        {
          "name": "fortuneteller"
        },
        {
          "name": "forum"
        },
        {
          "name": "fountain"
        },
        {
          "name": "fugitive"
        },
        {
          "name": "gardens"
        },
        {
          "name": "gear"
        },
        {
          "name": "ghostship"
        },
        {
          "name": "giant"
        },
        {
          "name": "gladiatorfortune"
        },
        {
          "name": "gold"
        },
        {
          "name": "golem"
        },
        {
          "name": "goons"
        },
        {
          "name": "governor"
        },
        {
          "name": "grandmarket"
        },
        {
          "name": "graverobber"
        },
        {
          "name": "greathall"
        },
        {
          "name": "groundskeeper"
        },
        {
          "name": "guide"
        },
        {
          "name": "haggler"
        },
        {
          "name": "hamlet"
        },
        {
          "name": "harbinger"
        },
        {
          "name": "harem"
        },
        {
          "name": "harvest"
        },
        {
          "name": "hauntedwoods"
        },
        {
          "name": "haven"
        },
        {
          "name": "herald"
        },
        {
          "name": "herbalist"
        },
        {
          "name": "hermit"
        },
        {
          "name": "hero"
        },
        {
          "name": "highway"
        },
        {
          "name": "hireling"
        },
        {
          "name": "hoard"
        },
        {
          "name": "hornofplenty"
        },
        {
          "name": "horsetraders"
        },
        {
          "name": "hovel"
        },
        {
          "name": "huntinggrounds"
        },
        {
          "name": "huntingparty"
        },
        {
          "name": "illgottengains"
        },
        {
          "name": "inheritance"
        },
        {
          "name": "inn"
        },
        {
          "name": "ironmonger"
        },
        {
          "name": "ironworks"
        },
        {
          "name": "island"
        },
        {
          "name": "jackofalltrades"
        },
        {
          "name": "jester"
        },
        {
          "name": "journeyman"
        },
        {
          "name": "junkdealer"
        },
        {
          "name": "keep"
        },
        {
          "name": "kingscourt"
        },
        {
          "name": "laboratory"
        },
        {
          "name": "labyrinth"
        },
        {
          "name": "legionary"
        },
        {
          "name": "library"
        },
        {
          "name": "lighthouse"
        },
        {
          "name": "loan"
        },
        {
          "name": "lookout"
        },
        {
          "name": "lostarts"
        },
        {
          "name": "lostcity"
        },
        {
          "name": "madman"
        },
        {
          "name": "magpie"
        },
        {
          "name": "mandarin"
        },
        {
          "name": "marauder"
        },
        {
          "name": "margrave"
        },
        {
          "name": "market"
        },
        {
          "name": "marketsquare"
        },
        {
          "name": "masquerade"
        },
        {
          "name": "masterpiece"
        },
        {
          "name": "menagerie"
        },
        {
          "name": "mercenary"
        },
        {
          "name": "merchantguild"
        },
        {
          "name": "merchant"
        },
        {
          "name": "merchantship"
        },
        {
          "name": "messenger"
        },
        {
          "name": "militia"
        },
        {
          "name": "mine"
        },
        {
          "name": "miningvillage"
        },
        {
          "name": "minion"
        },
        {
          "name": "mint"
        },
        {
          "name": "miser"
        },
        {
          "name": "mission"
        },
        {
          "name": "moat"
        },
        {
          "name": "moneylender"
        },
        {
          "name": "monument"
        },
        {
          "name": "mountainpass"
        },
        {
          "name": "mountebank"
        },
        {
          "name": "museum"
        },
        {
          "name": "mystic"
        },
        {
          "name": "nativevillage"
        },
        {
          "name": "navigator"
        },
        {
          "name": "necropolis"
        },
        {
          "name": "noblebrigand"
        },
        {
          "name": "nobles"
        },
        {
          "name": "nomadcamp"
        },
        {
          "name": "oasis"
        },
        {
          "name": "obelisk"
        },
        {
          "name": "oracle"
        },
        {
          "name": "orchard"
        },
        {
          "name": "outpost"
        },
        {
          "name": "overgrownestate"
        },
        {
          "name": "overlord"
        },
        {
          "name": "page"
        },
        {
          "name": "page"
        },
        {
          "name": "palace"
        },
        {
          "name": "pathfinding"
        },
        {
          "name": "patricianemporium"
        },
        {
          "name": "pawn"
        },
        {
          "name": "pearldiver"
        },
        {
          "name": "peasant"
        },
        {
          "name": "peddler"
        },
        {
          "name": "philosophersstone"
        },
        {
          "name": "pilgrimage"
        },
        {
          "name": "pillage"
        },
        {
          "name": "pirateship"
        },
        {
          "name": "plan"
        },
        {
          "name": "platinum"
        },
        {
          "name": "plaza"
        },
        {
          "name": "poacher"
        },
        {
          "name": "poorhouse"
        },
        {
          "name": "port"
        },
        {
          "name": "possession"
        },
        {
          "name": "potion"
        },
        {
          "name": "prince"
        },
        {
          "name": "princess"
        },
        {
          "name": "procession"
        },
        {
          "name": "province"
        },
        {
          "name": "quarry"
        },
        {
          "name": "quest"
        },
        {
          "name": "rabble"
        },
        {
          "name": "raid"
        },
        {
          "name": "ranger"
        },
        {
          "name": "ratcatcher"
        },
        {
          "name": "rats"
        },
        {
          "name": "raze"
        },
        {
          "name": "rebuild"
        },
        {
          "name": "relic"
        },
        {
          "name": "remake"
        },
        {
          "name": "remodel"
        },
        {
          "name": "ritual"
        },
        {
          "name": "rogue"
        },
        {
          "name": "royalblacksmith"
        },
        {
          "name": "royalcarriage"
        },
        {
          "name": "royalseal"
        },
        {
          "name": "ruinedlibrary"
        },
        {
          "name": "ruinedmarket"
        },
        {
          "name": "ruinedvillage"
        },
        {
          "name": "saboteur"
        },
        {
          "name": "sacrifice"
        },
        {
          "name": "sage"
        },
        {
          "name": "salttheearth"
        },
        {
          "name": "salvager"
        },
        {
          "name": "save"
        },
        {
          "name": "scavenger"
        },
        {
          "name": "scheme"
        },
        {
          "name": "scoutingparty"
        },
        {
          "name": "scout"
        },
        {
          "name": "scryingpool"
        },
        {
          "name": "seahag"
        },
        {
          "name": "seaway"
        },
        {
          "name": "secretchamber"
        },
        {
          "name": "sentry"
        },
        {
          "name": "settlersbustlingvillage"
        },
        {
          "name": "shantytown"
        },
        {
          "name": "silkroad"
        },
        {
          "name": "silver"
        },
        {
          "name": "sirbailey"
        },
        {
          "name": "sirdestry"
        },
        {
          "name": "sirmartin"
        },
        {
          "name": "sirmichael"
        },
        {
          "name": "sirvander"
        },
        {
          "name": "smithy"
        },
        {
          "name": "smugglers"
        },
        {
          "name": "soldier"
        },
        {
          "name": "soothsayer"
        },
        {
          "name": "spicemerchant"
        },
        {
          "name": "spoils"
        },
        {
          "name": "spy"
        },
        {
          "name": "squire"
        },
        {
          "name": "stables"
        },
        {
          "name": "stash"
        },
        {
          "name": "steward"
        },
        {
          "name": "stonemason"
        },
        {
          "name": "storeroom"
        },
        {
          "name": "storyteller"
        },
        {
          "name": "survivors"
        },
        {
          "name": "swamphag"
        },
        {
          "name": "swindler"
        },
        {
          "name": "tactician"
        },
        {
          "name": "talisman"
        },
        {
          "name": "tax"
        },
        {
          "name": "taxman"
        },
        {
          "name": "teacher"
        },
        {
          "name": "temple"
        },
        {
          "name": "thief"
        },
        {
          "name": "throneroom"
        },
        {
          "name": "tomb"
        },
        {
          "name": "torturer"
        },
        {
          "name": "tournament"
        },
        {
          "name": "tower"
        },
        {
          "name": "trade"
        },
        {
          "name": "trader"
        },
        {
          "name": "traderoute"
        },
        {
          "name": "tradingpost"
        },
        {
          "name": "training"
        },
        {
          "name": "transmogrify"
        },
        {
          "name": "transmute"
        },
        {
          "name": "travellingfair"
        },
        {
          "name": "treasurehunter"
        },
        {
          "name": "treasuremap"
        },
        {
          "name": "treasuretrove"
        },
        {
          "name": "treasury"
        },
        {
          "name": "tribute"
        },
        {
          "name": "triumphalarch"
        },
        {
          "name": "triumph"
        },
        {
          "name": "trustysteed"
        },
        {
          "name": "tunnel"
        },
        {
          "name": "university"
        },
        {
          "name": "upgrade"
        },
        {
          "name": "urchin"
        },
        {
          "name": "vagrant"
        },
        {
          "name": "vassal"
        },
        {
          "name": "vault"
        },
        {
          "name": "venture"
        },
        {
          "name": "village"
        },
        {
          "name": "villa"
        },
        {
          "name": "vineyard"
        },
        {
          "name": "walledvillage"
        },
        {
          "name": "wall"
        },
        {
          "name": "wanderingminstrel"
        },
        {
          "name": "warehouse"
        },
        {
          "name": "warrior"
        },
        {
          "name": "watchtower"
        },
        {
          "name": "wedding"
        },
        {
          "name": "wharf"
        },
        {
          "name": "wildhunt"
        },
        {
          "name": "windfall"
        },
        {
          "name": "winemerchant"
        },
        {
          "name": "wishingwell"
        },
        {
          "name": "witch"
        },
        {
          "name": "wolfden"
        },
        {
          "name": "woodcutter"
        },
        {
          "name": "workersvillage"
        },
        {
          "name": "workshop"
        },
        {
          "name": "youngwitch"
        }
  ];
    