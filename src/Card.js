import React from 'react';
import './CardList.css';

const Card = (props) => {
    let { name } = props;
    let source = `/card-art/${name}.jpg`;
    let alt = `card-named-${name}`
    return (
        <div className='dom-card dib br3 pa3 ma2 grow bw2 shadow-5'>
            <img alt={alt} src={source}></img>
            {/* <div>
                <p>Combat / Tempo</p>
            </div> */}
        </div>
    );
}

export default Card;