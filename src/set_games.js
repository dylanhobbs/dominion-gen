export const games = {
    "base": [ 
        {
            "name" : "First Game",
            "cards" : ['cellar', 'market', 'merchant', 'militia', 'mine', 'moat', 'remodel','smithy',
            'village','workshop']
        }, 
        {
            "name" : "Size Distortion",
            "cards" : ['artisan', 'bandit', 'bureaucrat', 'chapel', 'festival', 'gardens', 
            'sentry', 'throneroom', 'witch', 'workshop']
        },
        {
            "name": "Deck Top",
            "cards": ['artisan', 'bureaucrat', 'councilroom', 'festival', 'harbinger', 'laboratory', 
            'moneylender', 'sentry', 'vassal', 'village']
        },
        {
            "name": "Slight of Hand",
            "cards": ['cellar', 'councilroom', 'festival', 'gardens', 'library', 'harbinger', 'militia', 
            'poacher', 'smithy', 'throneroom']
        },
        {
            "name": "Improvements",
            "cards": ['artisan', 'cellar', 'market', 'merchant', 'mine', 'moat', 'moneylender', 
            'poacher', 'remodel', 'witch']
        },
        {
            "name": "Silver & Gold",
            "cards": ['bandit', 'bureaucrat', 'chapel', 'harbinger', 'laboratory', 'merchant', 'mine', 
            'moneylender', 'throneroom', 'vassal']
        }
    ],
    "Intrigue" : [

    ]
}