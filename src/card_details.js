export const cards = {
	"base" : { 
		"core": [
			{
				"name": "cellar",
				"cost": 2,
				"type": "Action",
				"draw": 1,
				"buy": 0,
				"tempo": 1,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0
		    },
		    {
				"name": "chapel",
				"cost": 2,
				"type": "Action",
				"draw": 0,
				"buy": 0,
				"tempo": 0,
				"offence": 0,
				"trash": 2,
				"money": 0,
				"victory": 0
		    },
		    {
				"name": "moat",
				"cost": 2,
				"type": "Action / Reaction",
				"draw": 1,
				"buy": 0,
				"tempo": 0,
				"offence": 1,
				"trash": 0,
				"money": 0,
				"victory": 0
		    },
		    {
				"name": "merchant",
				"cost": 3,
				"type": "action",
				"draw": 1,
				"buy": 0,
				"tempo": 1,
				"offence": 0,
				"trash": 0,
				"money": 1,
				"victory": 0
		    },
		    {
				"name": "vassal",
				"cost": 3,
				"type": "action",
				"draw": 0,
				"buy": 0,
				"tempo": 1,
				"offence": 0,
				"trash": 0,
				"money": 2,
				"victory": 0
		    },
		    {
				"name": "village",
				"cost": 3,
				"type": "action",
				"draw": 1,
				"buy": 0,
				"tempo": 2,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0
		    },
		    {
				"name": "workshop",
				"cost": 3,
				"type": "action",
				"draw": 0,
				"buy": 3,
				"tempo": 0,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0		  
			},
			{
				"name": "harbinger",
				"cost": 3,
				"type": "action",
				"draw": 1,
				"buy": 0,
				"tempo": 2,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0
			},
		    {
				"name": "bureaucrat",
				"cost": 4,
				"type": "Action / Attack",
				"draw": 0,
				"buy": 0,
				"tempo": 0,
				"offence": 2,
				"trash": 0,
				"money": 2,
				"victory": 0			  
		    },
		    {
				"name": "gardens",
				"cost": 4,
				"type": "Victory",
				"draw": 0,
				"buy": 0,
				"tempo": 0,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 4			  
		    },
		    {
				"name": "militia",
				"cost": 4,
				"type": "Action / Attack",
				"draw": 0,
				"buy": 0,
				"tempo": 0,
				"offence": 2,
				"trash": 0,
				"money": 2,
				"victory": 0			  
		    },
		    {
				"name": "moneylender",
				"cost": 4,
				"type": "action",
				"draw": 0,
				"buy": 0,
				"tempo": 0,
				"offence": 0,
				"trash": 1,
				"money": 3,
				"victory": 0		  
		    },
		    {
				"name": "poacher",
				"cost": 4,
				"type": "action",
				"draw": 1,
				"buy": 0,
				"tempo": 2,
				"offence": 0,
				"trash": 0,
				"money": 1,
				"victory": 0			  
		    },
		    {
				"name": "remodel",
				"cost": 4,
				"type": "action",
				"draw": 0,
				"buy": 2,
				"tempo": 0,
				"offence": 0,
				"trash": 2,
				"money": 0,
				"victory": 0
		    },
		    {
				"name": "throneroom",
				"cost": 4,
				"type": "action",
				"draw": 4,
				"buy": 0,
				"tempo": 0,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0
		    },
		    {
				"name": "bandit",
				"cost": 5,
				"type": "Action / Attack",
				"draw": 0,
				"buy": 0,
				"tempo": 0,
				"offence": 2,
				"trash": 0,
				"money": 3,
				"victory": 0		  
		    },
		    {
				"name": "councilroom",
				"cost": 5,
				"type": "action",
				"draw": 4,
				"buy": 1,
				"tempo": 0,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0	  
		    },
		    {
				"name": "festival",
				"cost": 5,
				"type": "action",
				"draw": 0,
				"buy": 2,
				"tempo": 1,
				"offence": 0,
				"trash": 0,
				"money": 2,
				"victory": 0		  
		    },
		    {
				"name": "laboratory",
				"cost": 5,
				"type": "action",
				"draw": 2,
				"buy": 0,
				"tempo": 3,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0			  
		    },
		    {
				"name": "library",
				"cost": 5,
				"type": "action",
				"draw": 5,
				"buy": 0,
				"tempo": 0,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0			  
		    },
		    {
				"name": "market",
				"cost": 5,
				"type": "action",
				"draw": 1,
				"buy": 1,
				"tempo": 2,
				"offence": 0,
				"trash": 0,
				"money": 1,
				"victory": 0			  
		    },
		    {
				"name": "mine",
				"cost": 5,
				"type": "action",
				"draw": 0,
				"buy": 0,
				"tempo": 0,
				"offence": 0,
				"trash": 2,
				"money": 3,
				"victory": 0			  
		    },
		    {
				"name": "sentry",
				"cost": 5,
				"type": "action",
				"draw": 1,
				"buy": 0,
				"tempo": 2,
				"offence": 0,
				"trash": 2,
				"money": 0,
				"victory": 0			  
			},
			{
				"name": "smithy",
				"cost": 4,
				"type": "action",
				"draw": 4,
				"buy": 0,
				"tempo": 0,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0
			},
		    {
				"name": "witch",
				"cost": 5,
				"type": "Action / Attack",
				"draw": 2,
				"buy": 0,
				"tempo": 0,
				"offence": 3,
				"trash": 0,
				"money": 0,
				"victory": 0			  
		    },
		    {
				"name": "artisan",
				"cost": 6,
				"type": "action",		  
				"draw": 0,
				"buy": 3,
				"tempo": 3,
				"offence": 0,
				"trash": 0,
				"money": 0,
				"victory": 0
		    },
		],
	    "removed" : [
	    	{
		      "name": "chancellor"
		    },
		    {
		      "name": "woodcutter",
			  "cost": 4,
			  "type": "action",
		    },
		    {
		      "name": "feast",
			  "cost": 4,
			  "type": "action",
		    },
		    {
		      "name": "spy",
			  "cost": 4,
			  "type": "Action / Attack",
		    },
		   	{
		      "name": "thief",
			  "cost": 4,
			  "type": "Action / Attack",
		    },
		    {
		      "name": "adventurer",
			  "cost": 6,
			  "type": "Action / Attack",
		    },
	    ]
	},
	"intrigue" : {
		"base" : [

		],
		"removed": [
			
		]
	}
}

