import React from 'react';
import Card from './Card';

const CardList = ({ cards }) => {
    const cardComp = cards.map((card, i) => {
        return (
            <Card key={i} name={card.name}/>
        );
    });

    return (
        <div className='card-container'>
            {cardComp}
        </div>
    )
}

export default CardList;